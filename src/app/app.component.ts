import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'angular-event-emitters';

  ngOnInit(){

  }

  // This method is called when the event from the child element is caught in the template
  displayCounter(count){
    console.log(count);
  }
}
