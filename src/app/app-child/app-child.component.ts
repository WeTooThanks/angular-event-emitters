import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './app-child.component.html',
  styleUrls: ['./app-child.component.css']
})
export class AppChildComponent implements OnInit {

  @Output() valueChange = new EventEmitter(); // emitted to parent on click event of child
  counter = 0; // passed as a parameter of the emitted event

  // called on the click event, emitting an event to be caught by the parent component
  valueChanged(){
    this.counter = this.counter + 1;
    this.valueChange.emit(this.counter);
  }

  constructor() { }

  ngOnInit() {
  }

}
