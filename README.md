# Angular Event Emmitters

Very simple application consisting of two components, a parent and a child.
This demonstrated the relationship between the two and how event emitters can be used to communicate between them.
Output goes to the web console of the browser.
